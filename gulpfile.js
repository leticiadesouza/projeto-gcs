// Todos os arquivos CSS que serão compactados
// Explicação: /*.css busca todos os arquivos css de uma pasta, /**/*.css busca todos os arquivos css de uma pasta e sub pasta.
var css = [
 './css-source/bootstrap/*.css',
 './css-source/style.css'
];
 
// Todos os arquivos JS que serão compactados
// Explicação: /*.js busca todos os arquivos css de uma pasta, /**/*.js busca todos os arquivos js de uma pasta e sub pasta.
var js  = [
    './js-source/vendor/jquery/*.js',         // Todos os arquivos do diretório Jquery
    './js-source/vendor/bootstrap/*.js',    // Todos os arquivos do diretório bootstrap e sub diretórios
    './js-source/main.js'                  // Arquivo único
];
 
// Núcleo do Gulp
var gulp = require('gulp');

// Transforma o javascript em formato ilegível para humanos
var uglify = require("gulp-uglify");

// Agrupa todos os arquivos em um
var concat = require("gulp-concat");

// Verifica alterações em tempo real, caso haja, compacta novamente todo o projeto 
var watch = require('gulp-watch');
 
// Minifica o CSS
var cssmin = require("gulp-cssmin");

// Remove comentários CSS
var stripCssComments = require('gulp-strip-css-comments');

var django = require('gulp-django-utils');

var optimizejs = require('gulp-optimize-js');

// Initialize application list for processing.
var apps = ['clinica_das_malas'];

// Initialize project with apps in current directory.
var project = new django.Project(apps);

var htmlmin = require('gulp-htmlmin');

gulp.task('minify', function() {
  return gulp.src('src/*.html')
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(gulp.dest('dist'));
});

// Processo que agrupará todos os arquivos CSS, removerá comentários CSS e minificará.
gulp.task('minify-css', function(){
    gulp.src(css)
    .pipe(concat('style.min.css'))
    .pipe(stripCssComments({all: true}))
    .pipe(cssmin())
    .pipe(gulp.dest('./css/'));
});

// Tarefa de minificação do Javascript
gulp.task('minify-js', function () {
    gulp.src(js)                        // Arquivos que serão carregados, veja variável 'js' no início
    .pipe(concat('script.min.js'))      // Arquivo único de saída
    .pipe(uglify())                     // Transforma para formato ilegível
    .pipe(gulp.dest('./js/'));          // pasta de destino do arquivo(s)
});

// Tarefa padrão quando executado o comando GULP
gulp.task('default',['minify-js','minify-css']);
 
// Tarefa de monitoração caso algum arquivo seja modificado, deve ser executado e deixado aberto, comando "gulp watch".
gulp.task('watch', function() {
    gulp.watch(js, ['minify-js']);
 gulp.watch(css, ['minify-css']);
});

gulp.task('optimize', function() {
  gulp.src('./js/minified.js')
    .pipe(optimizejs(options))
    .pipe(gulp.dest('./dist/'))
});

// Create a task which depends on the same tasks in apps.
project.task('js', function() {
  // Take all `.js` files from `django-project/static/main/js`,
  // concatenate it and put to `django-project/static/build`.
  project.src('static/main/js/*.js')
    .pipe(concat('main.js'))
    .pipe(project.dest('static/build'));
});
