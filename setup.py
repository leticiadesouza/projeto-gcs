from setuptools import setup, find_packages

with open('requirements.txt') as f:
    requirements = f.read().splitlines()

setup(name='clinica_das_malas',
      version='1.0',
      description='O projeto é focado em realizar a automação de um software para uma loja de conserto de malas.',
      long_description='The project is focused on performing the automation of software for a bag repair shop.',
      author='Ateldy Borges Brasil Filho, Letícia de Souza Santos',
      install_requires=requirements,
      license='MIT License',
      platforms='Web',
      author_email='ateldybfilho@gmail.com, leticia.souza212@gmail.com',
      url='https://gcs-clinica-das-malas.herokuapp.com/',
      packages=find_packages(),
      )