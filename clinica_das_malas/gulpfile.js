var django = require('gulp-django-utils');
var concat = require('gulp-concat');

module.exports = function(project) {
  // Initialize application in project.
  var app = new django.Application('clinica_das_malas', project);

  // Create task in application namespace.
  app.task('js', function() {
    // Take all `.js` files from `projeto-gcs/clinica_das_malas/static/clinica_das_malas/js`,
    // concatenate it and put to `projeto-gcs/static/build`.
    app.src('static/clinica_das_malas/js/*.js')
      .pipe(concat('clinica_das_malas.js'))
      .pipe(project.dest('static/build'));
  });
};
