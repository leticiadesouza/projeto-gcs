from django.apps import AppConfig


class TiposervicoConfig(AppConfig):
    name = 'clinica_das_malas.tipoServico'
