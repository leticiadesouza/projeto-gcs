from django.contrib import admin

# Register your models here.
from clinica_das_malas.tipoServico.models import TipoServico

admin.site.register(TipoServico)