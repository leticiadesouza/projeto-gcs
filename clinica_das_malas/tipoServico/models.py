from django.db import models

# Create your models here.
from clinica_das_malas.produto.models import Produto


class TipoServico(models.Model):
    nome = models.CharField(max_length=40)
    descricao = models.TextField()
    valor = models.FloatField()
    tempo_servico = models.IntegerField()

    produtos = models.ManyToManyField(Produto)

    def __str__(self):
        return self.nome