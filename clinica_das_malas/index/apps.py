from django.apps import AppConfig


class IndexConfig(AppConfig):
    name = 'clinica_das_malas.index'
