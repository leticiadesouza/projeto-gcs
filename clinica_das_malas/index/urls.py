from .views import index
from django.conf.urls import url

urlpatterns = [
    url(r'^$', index, name='index'),
    # url(r'^contato/$', contato, name='contato'),
    # url(r'^email', send_email, name="email"),
]
