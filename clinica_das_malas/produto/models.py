from django.db import models

# Create your models here.
class Produto(models.Model):
    nome = models.CharField(max_length=30)
    descricao = models.TextField()
    quantidade = models.IntegerField()
    qtd_min = models.IntegerField()

    created_at = models.DateTimeField('Criado em', auto_now_add=True)
    updated_at = models.DateTimeField('Atualizado em', auto_now=True)


    def __str__(self):
        return self.nome

    def get_status(self):
        if self.quantidade > self.qtd_min:
            return 'ok'
        elif self.quantidade == self.qtd_min:
            return 'cuidado'
        elif self.quantidade < self.qtd_min:
            return 'faltando'
        else:
            return 'all'