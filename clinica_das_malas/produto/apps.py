from django.apps import AppConfig


class ProdutoConfig(AppConfig):
    name = 'clinica_das_malas.produto'
