from django.contrib.auth.decorators import login_required
from django.shortcuts import render

# Create your views here.
from clinica_das_malas.produto.models import Produto


@login_required()
def estoque_list(request):
    produtos = Produto.objects.all()
    context = {
        'produtos': produtos
    }
    return render(request, 'estoque.html', context)