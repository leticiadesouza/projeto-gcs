from django.conf.urls import url
from .views import estoque_list

urlpatterns = [
    url(r'^$', estoque_list, name="list"),
]
