import pytest

from clinica_das_malas.produto.models import Produto


def test_get_status_cuidado():
    produto = Produto()
    produto.qtd_min = 2
    produto.quantidade = 2
    produto.nome = "nomeProduto"
    produto.descricao = "descricao"
    assert produto.get_status() == "cuidado"


def test_get_status_faltando():
    produto = Produto()
    produto.qtd_min = 2
    produto.quantidade = 1
    produto.nome = "nomeProduto"
    produto.descricao = "descricao"
    assert produto.get_status() == "faltando"


def test_get_status_ok():
    produto = Produto()
    produto.qtd_min = 2
    produto.quantidade = 3
    produto.nome = "nomeProduto"
    produto.descricao = "descricao"
    assert produto.get_status() == "ok"


# @pytest.mark.django_db
# def test_save_produto():
#     produto = Produto()
#     produto.qtd_min = 2
#     produto.quantidade = 3
#     produto.nome = "nomeProduto"
#     produto.descricao = "descricao"
#     produto.save()
#     assert produto.nome == "nomeProduto"
