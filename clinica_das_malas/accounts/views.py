from django.shortcuts import render, redirect

from django.conf import settings
from clinica_das_malas.accounts.forms import UserAdminCreationForm
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required


def register(request):
    template_name = 'accounts/register.html'
    if request.method == 'POST':
        form = UserAdminCreationForm(request.POST)
        if form.is_valid():
            user = form.save()
            user = authenticate(username=user.username, password=form.cleaned_data['password1'])
            login(request, user)
            return redirect('core:home')
    else:
        form = UserAdminCreationForm()
    context = {
        'form': form
    }

    return render(request, template_name, context)


@login_required()
def conta(request):
    return render(request, 'accounts/conta.html')


@login_required()
def messages(request):
    return render(request, 'accounts/mensagens.html')