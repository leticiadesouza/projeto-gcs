from django.conf.urls import url
from django.contrib.auth.views import login, logout
from clinica_das_malas.accounts.views import register, conta, messages
from clinica_das_malas.settings import LOGOUT_URL

urlpatterns = [
    url(r'^entrar/', login, {'template_name': 'accounts/login.html'}, name='login'),
    url(r'^sair/', logout, {'next_page': LOGOUT_URL}, name='logout'),
    url(r'^cadastre-se/$', register, name='register'),
    url(r'^mensagens/$', messages, name='mensagens'),
    url(r'^$', conta, name ='conta'),
]
