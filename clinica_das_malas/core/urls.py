from django.conf.urls import url

from clinica_das_malas.core.views import home, contato, send_email


urlpatterns = [
    url(r'^$', home, name='home'),
    url(r'^contato/$', contato, name='contato'),
    url(r'^index', send_email, name="email"),
]
