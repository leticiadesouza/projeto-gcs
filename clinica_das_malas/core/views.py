import json as simplejson
from datetime import datetime, timedelta
from django.shortcuts import render

from clinica_das_malas.bagagem.models import Bagagem
from .forms import FormEmail
from django.core.mail import send_mail


def home(request):

    if request.user.is_authenticated():
        if request.user.is_superuser:
            entrada_total_mes1 = 0
            entrada_total_mes2 = 0
            entrada_total_mes3 = 0
            entrada_total_mes4 = 0
            bagagens_month1 = Bagagem.objects.filter(created_at__month=get_previous_month(0))
            bagagens_month2 = Bagagem.objects.filter(created_at__month=get_previous_month(1))
            bagagens_month3 = Bagagem.objects.filter(created_at__month=get_previous_month(2))
            bagagens_month4 = Bagagem.objects.filter(created_at__month=get_previous_month(3))

            for bagagem in bagagens_month1:
                entrada_total_mes1 += bagagem.get_valor()
            entrada_total_mes1 = simplejson.dumps(entrada_total_mes1)# Viabilizando utilização no js

            for bagagem in bagagens_month2:
                entrada_total_mes2 += bagagem.get_valor()
            entrada_total_mes2 = simplejson.dumps(entrada_total_mes2)# Viabilizando utilização no js

            for bagagem in bagagens_month3:
                entrada_total_mes3 += bagagem.get_valor()
            entrada_total_mes3 = simplejson.dumps(entrada_total_mes3)# Viabilizando utilização no js

            for bagagem in bagagens_month4:
                entrada_total_mes4 += bagagem.get_valor()
            entrada_total_mes4 = simplejson.dumps(entrada_total_mes4)# Viabilizando utilização no js

            context = {'user': request.user,
                       'valor1': entrada_total_mes1,
                       'valor2': entrada_total_mes2,
                       'valor3': entrada_total_mes3,
                       'valor4': entrada_total_mes4}
        else:
            context = {'user': request.user}

        return render(request, 'inicio.html', context)
    else:
        return render(request, 'index.html')


def contato(request):
    context = {}
    return render(request, 'contato.html', context)


def send_email(request):
    template_name = 'index.html'
    context = {}

    if request.method == 'POST':
        form = FormEmail(request.POST)
        if form.is_valid():
            context['is_valid'] = True
            send_mail(
                form.cleaned_data['name'],
                'Nome: ' + form.cleaned_data['name'] + '\n' + 'De: ' + form.cleaned_data['email'] + '\n\n\n' + 'Mensagem: ' + form.cleaned_data['mensagem'],
                form.cleaned_data['email'],
                ['admclinicadasmalas@gmail.com '],
                fail_silently=False,
            )
            form = FormEmail()#Limpando o formulário novamente
    else:
        form = FormEmail()
    context['form'] = form
    return render(request, template_name, context)


def get_previous_month(qtd):
    i=0
    prev = datetime.today()
    for i in range(qtd):
        prev = prev.replace(day=1) - timedelta(days=1)
    print(prev)
    return prev.month