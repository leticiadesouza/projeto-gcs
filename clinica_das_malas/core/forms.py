from django import forms

class FormEmail(forms.Form):
    name = forms.CharField(label='Nome', max_length=50)
    email = forms.EmailField(label='Email')
    mensagem = forms.CharField(label='Mensagem', widget=forms.Textarea)
