from django.conf.urls import url

from clinica_das_malas.tecnico.views import historico, pesquisar, geral

urlpatterns = [
    url(r'^(\d+)', historico, name="historico"),
    url(r'^pesquisa', pesquisar, name="pesquisar"),
    url(r'^geral', geral, name="geral")

]