from django.apps import AppConfig


class TecnicoConfig(AppConfig):
    name = 'clinica_das_malas.tecnico'
