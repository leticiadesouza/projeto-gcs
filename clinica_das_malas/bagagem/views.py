from django.shortcuts import render, get_object_or_404

from django.http import HttpResponse

from clinica_das_malas.bagagem.models import Bagagem
from django.contrib.auth.decorators import login_required
from clinica_das_malas.bagagem.forms import BagagemForm


from clinica_das_malas.tipoServico.models import TipoServico


@login_required()
def list_bagagens(request):
    bagagens = Bagagem.objects.all()
    context = {
        'bagagens': bagagens
    }
    return render(request, 'list_bagagens.html', context)

@login_required()
def show_bagagem(request, pk):
    bagagem = get_object_or_404(Bagagem, pk=pk)
    servicos = bagagem.tipo_servicos.all()
    total = 0
    for servico in servicos:
        total += servico.valor
    context = {
        'bagagem': bagagem,
        'servicos': servicos,
        'total': total
    }
    return render(request, 'show_bagagem.html', context)


@login_required()
def cliente_bagagens(request):
    bagagens = Bagagem.objects.filter(cliente=request.user)
    user = request.user
    context = {'bagagens':bagagens,
               'user':user}
    return render(request, 'cliente_bagagens.html', context)


@login_required()
def entra_bagagem(request):
    template_name = 'entra_bagagem.html'
    tipos = TipoServico.objects.all()
    if request.method == 'POST':
        form = BagagemForm(request.POST)
        print(form.errors)
        if form.is_valid():
            print('olha que entrou')
            bagagem = form.save()
            bagagem.save()

    else:
        form = BagagemForm()
    context = {
        'form': form,
        'tipos': tipos
    }

    return render(request, template_name, context)
