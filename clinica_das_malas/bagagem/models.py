from datetime import date

from django.db import models
from django.contrib.auth.models import User

from clinica_das_malas.tecnico.models import Tecnico
from clinica_das_malas.tipoServico.models import TipoServico
from clinica_das_malas.accounts.models import User


class Bagagem(models.Model):
    TIPO_BAGAGENS = (
    ('B', 'Bolsa'),
    ('M', 'Mala')
    )

    TIPO_MALA = (
    ('F', 'Flexível'),
    ('S', 'Semi-rígida'),
    ('R', 'Rígida')
    )

    TAMANHOS = (
    ('P', 'Pequena'),
    ('M', 'Média'),
    ('G', 'Grande')
    )

    MODELOS = (
    ('N', 'Normal'),
    ('3', '360')
    )

    cliente = models.ForeignKey(User)

    tipo_bagagem = models.CharField(
    max_length = 1,
    choices=TIPO_BAGAGENS
    )
    marca = models.CharField(max_length = 30)
    cor = models.CharField(max_length = 20)
    tipo_mala = models.CharField(
        max_length = 1,
        choices = TIPO_MALA
        )
    tamanhos = models.CharField(
        max_length = 1,
        choices = TAMANHOS
        )
    modelos = models.CharField(
        max_length = 1,
        choices = MODELOS
        )
    tipo_servicos = models.ManyToManyField(TipoServico)
    prazo = models.IntegerField()
    observacoes = models.TextField(blank=True, null=True)
    tecnico = models.ForeignKey(Tecnico, null=True)

    concluida = models.BooleanField(default=False)

    created_at = models.DateTimeField('Criado em', auto_now_add=True) #Atualiza sempre que for criado
    updated_at = models.DateTimeField('Atualizado em', auto_now=True)

    def __str__(self):
        return self.cliente.username + " - " + self.marca + ' - ' + self.cor

    def get_future_date(self):
        futuro = date.fromordinal(self.created_at.toordinal()+self.prazo)
        return futuro

    def get_valor(self):
        servicos = self.tipo_servicos.all()
        total = 0
        for servico in servicos:
            total += servico.valor
        return total

    def get_status(self):
        if self.concluida:
            return 'pronto'
        elif date.today() > self.get_future_date():
            return 'atrasado'
        else:
            return 'pendente'

    class Meta:
        ordering = ['created_at']
