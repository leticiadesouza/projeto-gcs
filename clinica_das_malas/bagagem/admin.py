from django.contrib import admin

from .models import Bagagem


class BagagemAdmin(admin.ModelAdmin):

	list_display = ['cliente', 'tipo_bagagem', 'tamanhos', 'cor', 'created_at', 'prazo']
	search_fields = ['cliente']


admin.site.register(Bagagem, BagagemAdmin)