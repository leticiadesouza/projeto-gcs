from django import forms

from clinica_das_malas.bagagem.models import Bagagem


class BagagemForm(forms.ModelForm):

    class Meta:
        model = Bagagem
        fields = ('cliente', 'tipo_bagagem', 'marca', 'cor', 'tipo_mala', 'tamanhos', 'modelos', 'tipo_servicos', 'prazo',
                  'observacoes', 'tecnico')