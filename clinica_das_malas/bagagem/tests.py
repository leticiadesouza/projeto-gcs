from django.test import TestCase

from clinica_das_malas.bagagem.models import Bagagem


def test_bagagem_get_status():
    bagagem = Bagagem()
    bagagem.concluida = True;
    assert bagagem.get_status() == "pronto"

