# Projeto Final de Gerência de Configuração de Software - Clínica das Malas
## Descrição do Projeto
O projeto é focado em realizar a automação de um software para uma loja de conserto de malas.

## Desenvolvedores
Ateldy Borges Brasil Filho - 15/0006101
Letícia de Souza Santos - 15/0015160

## Links
### Heroku
https://gcs-clinica-das-malas.herokuapp.com/

## Como rodar

	$ pip install -r requirements.txt

	$ python3 manage.py runserver

acesse 127.0.0.1:8000

### Docker

	$ docker build .

	$ docker-compose build

	$ docker-compose up

### GULP

Comando para instalar todas as dependências do gulp

	$ npm install --save-dev

Então para, como default, minifica js e css

	$ gulp

### INSTALAR DEPENDENCIAS COM EGG

Para instalar as dependências do projeto, entre na pasta dist

	$ cd dist/

Rode o comando

	$ sudo easy_install clinica_das_malas-1.0-py3.5.egg
